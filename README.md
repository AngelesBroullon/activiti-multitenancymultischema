# Activiti Multitenancy POC (Proof of Concept)

The purpose of this project is to compare the existing Multitenancy solution against a Multischema solution (see [Jorambarrez Blog](http://www.jorambarrez.be/blog/2015/10/06/multi-tenancy-separate-database-schemas-in-activiti))

The Java Maven project uses two workflow engines with dedicated in-memory data stores. 
* There are two workflow definitions (onboardin v1 and onboarding v2):
  * onboarding_v1.bpmn20.xml
  * onboarding_v2.bpmn20.xml

* We also provide 2 datasets for workflow executions:
  * onboarding_data_1.txt
  * onboarding_data_2.txt
 
## Test files
* There are integration tests that will allow you to run tests to check how both versions work.
* Junit tests were also added.
