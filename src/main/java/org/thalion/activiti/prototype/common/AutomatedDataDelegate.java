package org.thalion.activiti.prototype.common;

import java.util.Date;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Delagate class
 * 
 * @author blazevicv
 */
public class AutomatedDataDelegate implements JavaDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(AutomatedDataDelegate.class);

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.activiti.engine.delegate.JavaDelegate#execute(org.activiti.engine.
     * delegate.DelegateExecution)
     */
    public void execute(DelegateExecution execution) throws Exception {
        Date now = new Date();
        execution.setVariable("autoWelcomeTime", now);
        LOGGER.info("Faux call to backend for [" + execution.getVariable("fullName") + "]");
    }

}
