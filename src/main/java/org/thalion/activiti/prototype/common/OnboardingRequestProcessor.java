package org.thalion.activiti.prototype.common;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormData;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.impl.form.DateFormType;
import org.activiti.engine.impl.form.LongFormType;
import org.activiti.engine.impl.form.StringFormType;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * The utility class to test with
 * 
 * Utility class that pushes onboarding workflow from beginning until the end.
 * Compatible with onboarding workflow versions 1 and 2. Reads required data
 * elements from provided input source.
 */
@Component("onboardingRequestProcessor")
public class OnboardingRequestProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(OnboardingRequestProcessor.class);

	/**
	 * Helper method to convert test data class path resource into an input stream
	 * 
	 * @param dataSetName resource name
	 * @return test data set input stream
	 */
	private InputStream getTestDataSet(final String dataSetName) {
		InputStream input = ClassLoader.getSystemResourceAsStream(dataSetName);

		LOGGER.info("Data set name:{}", dataSetName);

		return input;
	}

	/**
	 * Encapsulate the process instance generation for no tenancy
	 * 
	 * @param processDefinition process definition to execute
	 * @param runtimeService    to manage the runtime execution
	 * @return the process instance generation for no tenancy
	 */
	public ProcessInstance getProcessInstance(ProcessDefinition processDefinition, RuntimeService runtimeService) {
		return runtimeService.startProcessInstanceByKey(processDefinition.getKey());
	}

	/**
	 * Creates and executes process definition instance on this.engine usind
	 * provided test data set.
	 * 
	 * @param processEngine     the process engine
	 * @param processDefinition process definition to execute
	 * @param testDataSet       test data set to be be used
	 * @throws ParseException in case something fails
	 */
	public void runProcess(final ProcessEngine processEngine, final ProcessDefinition processDefinition,
			final String testDataSet) throws ParseException {

		RuntimeService runtimeService = processEngine.getRuntimeService();
		ProcessInstance processInstance = getProcessInstance(processDefinition, runtimeService);

		if (processInstance != null) {
			LOGGER.info(
					"Onboarding process started with process instance id: [{}], key: [{}], process definition id: [{}]",
					processInstance.getProcessInstanceId(), processInstance.getProcessDefinitionKey(),
					processDefinition.getId());
		} else {
			LOGGER.info("Onboarding process is null");
		}

		// Get task
		TaskService taskService = processEngine.getTaskService();
		FormService formService = processEngine.getFormService();
		HistoryService historyService = processEngine.getHistoryService();

		// Scanner scanner = new Scanner(System.in);
		Scanner scanner = new Scanner(getTestDataSet(testDataSet));

		while (processInstance != null && !processInstance.isEnded()) {
			List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup("managers").list();
			LOGGER.info("Active outstanding tasks: [" + tasks.size() + "]");
			for (int i = 0; i < tasks.size(); i++) {
				Task task = tasks.get(i);
				LOGGER.info("Processing Task [" + task.getName() + "]");
				Map<String, Object> variables = new HashMap<String, Object>();
				FormData formData = formService.getTaskFormData(task.getId());
				for (FormProperty formProperty : formData.getFormProperties()) {
					if (StringFormType.class.isInstance(formProperty.getType())) {
						LOGGER.info(formProperty.getName() + "?");
						String value = scanner.nextLine();
						LOGGER.info(">{}", value);
						variables.put(formProperty.getId(), value);
					} else if (LongFormType.class.isInstance(formProperty.getType())) {
						LOGGER.info(formProperty.getName() + "? (Must be a whole number)");
						Long value = Long.valueOf(scanner.nextLine());
						LOGGER.info(">{}", value.toString());
						variables.put(formProperty.getId(), value);
					} else if (DateFormType.class.isInstance(formProperty.getType())) {
						LOGGER.info(formProperty.getName() + "? (Must be a date m/d/yy)");
						DateFormat dateFormat = new SimpleDateFormat("m/d/yy");
						Date value = dateFormat.parse(scanner.nextLine());
						LOGGER.info(">{}", value.toString());
						variables.put(formProperty.getId(), value);
					} else {
						LOGGER.info("<form type not supported>");
					}
				}
				taskService.complete(task.getId(), variables);

				HistoricActivityInstance endActivity = null;
				List<HistoricActivityInstance> activities = historyService.createHistoricActivityInstanceQuery()
						.processInstanceId(processInstance.getId()).finished().orderByHistoricActivityInstanceEndTime()
						.asc().list();
				for (HistoricActivityInstance activity : activities) {
					if (activity.getActivityType() == "startEvent") {
						LOGGER.info("BEGIN " + processDefinition.getName() + " ["
								+ processInstance.getProcessDefinitionKey() + "] " + activity.getStartTime());
					}
					if (activity.getActivityType() == "endEvent") {
						// Handle edge case where end step happens so fast that
						// the end step
						// and previous step(s) are sorted the same. So, cache
						// the end step
						// and display it last to represent the logical
						// sequence.
						endActivity = activity;
					} else {
						LOGGER.info("-- " + activity.getActivityName() + " [" + activity.getActivityId() + "] "
								+ activity.getDurationInMillis() + " ms");
					}
				}
				if (endActivity != null) {
					LOGGER.info("-- " + endActivity.getActivityName() + " [" + endActivity.getActivityId() + "] "
							+ endActivity.getDurationInMillis() + " ms");
					LOGGER.info("COMPLETE " + processDefinition.getName() + " ["
							+ processInstance.getProcessDefinitionKey() + "] " + endActivity.getEndTime());
				}
			}
			// Re-query the process instance, making sure the latest state is
			// available
			processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstance.getId())
					.singleResult();

		}
		scanner.close();
	}

}
