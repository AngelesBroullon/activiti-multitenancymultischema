package org.thalion.activiti.prototype.common;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Angeles Broullon
 * 
 *         Extends the original request processor in order to handle the
 *         multi-tenancy for every processor
 *
 */
@Component("onboardingRequestProcessorWithTenant")
public class OnboardingRequestProcessorWithTenant extends OnboardingRequestProcessor {

	public OnboardingRequestProcessorWithTenant() {
		super();
	}

	/**
	 * Encapsulate the process instance generation for no tenancy
	 * 
	 * @param processDefinition process definition to execute
	 * @param runtimeService    to manage the runtime execution
	 * @return the process instance generation for no tenancy
	 */
	@Override
	public ProcessInstance getProcessInstance(ProcessDefinition processDefinition, RuntimeService runtimeService) {
		return runtimeService.startProcessInstanceByKeyAndTenantId(processDefinition.getKey(),
				processDefinition.getTenantId());
	}
}
