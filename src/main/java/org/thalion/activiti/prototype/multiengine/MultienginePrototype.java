package org.thalion.activiti.prototype.multiengine;

import java.text.ParseException;
import java.util.Set;

import org.activiti.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.thalion.activiti.prototype.common.OnboardingRequestProcessor;

/**
 * 
 * Inherited code
 * 
 * Main POC class to be tested by JUnit. Contains wrapper methods and predefined
 * config elements to simplify POC execution.
 * 
 */
@Component
public class MultienginePrototype {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MultienginePrototype.class);

	@Autowired
	private ProcessEngineStorage engineStorage;
	
	@Autowired
	@Qualifier("onboardingRequestProcessor")
	private OnboardingRequestProcessor processor;

	/**
	 * Adds engine with given name to internal engine store
	 * 
	 * @param engineName engine name
	 * @return engine store
	 */
	public void addEngine(String engineName) {
		engineStorage.addEngine(engineName);
	}
	
	/**
	 * Removes all the process engines
	 */
	public void clearEngines() {
		engineStorage.clear();
	}
	
	/**
	 * Gets the engine names from the storage
	 * @return the engine names from the storage
	 */
	public Set<String> getEngineNames(){
		return engineStorage.getEngineNames();
	}

	/**
	 * @param engineName
	 * @param workflowDefinition
	 * @return
	 */
	public ProcessDefinition deployWorkflow(final String engineName, final String workflowDefinition) {
		return engineStorage.deployEngineProcess(engineName, workflowDefinition);
	}

	/**
	 * Executs workflow deployed on given workflow engine with defined data set
	 * 
	 * @param engineName  workflow engine name in engine store
	 * @param processKey  workflow definition key
	 * @param dataSetName data set name / resource
	 * @return workflow processor
	 * @throws ParseException
	 */
	public OnboardingRequestProcessor runLatestWorkflowWithKey(final String engineName, final String processKey,
			final String dataSetName) throws ParseException {

		ProcessDefinition processDefinition = engineStorage.findLatestProcessDefinition(engineName, processKey);

		if (processDefinition == null) {
			LOGGER.error("Cannot find process definition with key: {} deployed on engine: {}", processKey, engineName);
			return null;
		}

		processor.runProcess(engineStorage.getProcessEngine(engineName), processDefinition, dataSetName);

		return processor;
	}
}
