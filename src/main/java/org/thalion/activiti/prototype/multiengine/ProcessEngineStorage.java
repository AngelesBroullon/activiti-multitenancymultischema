package org.thalion.activiti.prototype.multiengine;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Factory class to build Activiti engine store. New engines are created with
 * default configuration and in-memory data store. Engines are named is used as
 * reference and db user name.
 * 
 */
@Component
public class ProcessEngineStorage {

	/**
	 * Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessEngineStorage.class);

	/**
	 * Driver for the database
	 */
	private static final String DRIVER = "org.h2.Driver";
	/**
	 * Password for the database
	 */
	private static final String PASSWORD = "";
	/**
	 * Database configuration string for jdbc
	 */
	private static final Object H2_CONFIG = "jdbc:h2:mem:";
	/**
	 * Delay for the databose close instruction
	 */
	private static final Object DELAY_STRING = ";DB_CLOSE_DELAY=1000";

	/**
	 * A map containing the different engines
	 */
	private HashMap<String, ProcessEngine> map;

	/**
	 * Constructor
	 */
	public ProcessEngineStorage() {
		map = new HashMap<>();
	}

	/**
	 * Method builds an engine with a dedicated name if such does not exists
	 * already. Otherwise returns one from the engine store.
	 * 
	 * @param engine name to be created.
	 * @return activiti process engine for given name
	 */
	public synchronized void addEngine(String engineName) {
		if (map.containsKey(engineName)) {
			LOGGER.info("Such Activit engine: {} already exists. Returning existing one: ", engineName);
		}

		LOGGER.info("Creating a new Activiti engine: {}", engineName);
		ProcessEngine processEngine = generateEngine(engineName);
		map.put(engineName, processEngine);

		String pName = processEngine.getName();
		String ver = ProcessEngine.VERSION;
		LOGGER.info("ProcessEngine [{}] Version: [{}] added to the storage with name: {}", pName, ver, engineName);
	}

	/**
	 * Generates the engine
	 * 
	 * @param engineName the name for the engine
	 * @return a new Activiti ProcessEngine
	 */
	private ProcessEngine generateEngine(String engineName) {
		ProcessEngineConfiguration processEngineConfiguration =  new StandaloneProcessEngineConfiguration();
		processEngineConfiguration.setJdbcUrl(buildEngineDBUrl(engineName)).setJdbcUsername(engineName).setJdbcPassword(PASSWORD)
				.setJdbcDriver(DRIVER)
				.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
				.setProcessEngineName(engineName);

		return processEngineConfiguration.buildProcessEngine();
	}
	

	/**
	 * Helper method to construct in-memory engine store JDBC URL
	 * 
	 * @param userName database user name
	 * @return JDBC string
	 */
	private static String buildEngineDBUrl(String userName) {
		StringBuilder builder = new StringBuilder();

		builder.append(H2_CONFIG);
		builder.append(userName.toLowerCase());
		builder.append(DELAY_STRING);

		return builder.toString();
	}

	/**
	 * Deploys workflow process definition on to workflow engine with given name
	 * 
	 * @param engineName target engine name
	 * @param resource   workflow process definition resource name
	 * @return workflow process definition entity reference
	 */
	public synchronized ProcessDefinition deployEngineProcess(final String engineName,
			final String processDefinitionResource) {
		if (!map.containsKey(engineName)) {
			LOGGER.error("Engine with name: {} NOT FOUND! Exit", engineName);
			return null;
		}

		RepositoryService repositoryService = map.get(engineName).getRepositoryService();
		Deployment deployment = repositoryService.createDeployment().addClasspathResource(processDefinitionResource)
				.deploy();
		ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
				.deploymentId(deployment.getId()).singleResult();

		LOGGER.info("Deployed process definition [{}] with id:[{}], key:[{}] in to engine: [{}]",
				processDefinition.getName(), processDefinition.getId(), processDefinition.getKey(), engineName);

		return processDefinition;
	}

	/**
	 * Retrieves workflow process definition from engine with given name and process
	 * key.
	 * 
	 * @param engineName workflow engine name
	 * @param processKey process key
	 * @return process definition, otherwise null if not found
	 */
	public synchronized ProcessDefinition findLatestProcessDefinition(final String engineName,
			final String processKey) {
		if (!map.containsKey(engineName)) {
			LOGGER.error("Engine with name: {} NOT FOUND! Exit", engineName);
			return null;
		}

		RepositoryService repositoryService = map.get(engineName).getRepositoryService();

		return repositoryService.createProcessDefinitionQuery().processDefinitionKey(processKey).singleResult();
	}

	/**
	 * Retrieves the stored engine names
	 * 
	 * @return the stored engine names
	 */
	public Set<String> getEngineNames() {
		return map.keySet();
	}

	/**
	 * Retrieves an engine from the storage
	 * 
	 * @param engineName the engine name, or the key
	 * @return retrives the engine, or its value
	 */
	public ProcessEngine getProcessEngine(String engineName) {
		return map.get(engineName);
	}

	/**
	 * Cleans up the map content
	 */
	public void clear() {
		for (String name : map.keySet()) {
			ProcessEngine engine = map.get(name);
			deleteAllDeployments(engine);
			engine.close();
		}
		map.clear();
	}

	/**
	 * Deletes all the deployments from an engine
	 * @param engine the process engine whose deployments we want to delete
	 */
	private void deleteAllDeployments(ProcessEngine engine) {
		RepositoryService repositoryService = engine.getRepositoryService();
		List<Deployment> deployments = repositoryService.createDeploymentQuery().list();
		for (Deployment deployment : deployments) {
			repositoryService.deleteDeployment(deployment.getId());
		}		
	}

}
