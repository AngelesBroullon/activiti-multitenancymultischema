package org.thalion.activiti.prototype.multischema;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.activiti.engine.impl.cfg.multitenant.TenantInfoHolder;

/**
 * 
 * @author Angeles Broullon
 * 
 *         A custom InfoHolder for testing purposes
 *
 */
public class CustomTenantInfoHolder implements TenantInfoHolder {
	
	// tenants for testing purposes
	public static final String TENANT_1 = "ACTIVITI-1";
	public static final String TENANT_2 = "ACTIVITI-2";

	/**
	 * A set with the tenant identifiers It is static in order to work as a
	 * singleton class
	 */
	private static Set<String> listTenantId;

	/**
	 * The default tenant identifier
	 */
	private String currentTenantId;

	/**
	 * Constructor
	 */
	public CustomTenantInfoHolder() {
		listTenantId = new HashSet<String>();
		clearCurrentTenantId();
	}
	
	/**
	 * Generates a default infoHolder for testing purposes
	 */
	public void loadDefaultCustomTenantInfoHolder() {
		addTenant(CustomTenantInfoHolder.TENANT_1);
		addTenant(CustomTenantInfoHolder.TENANT_2);
		setCurrentTenantId(CustomTenantInfoHolder.TENANT_1);
	}

	/**
	 * Gets all the tenant identifiers
	 * 
	 * @return a Collection with all the tenant identifiers
	 */
	public Collection<String> getAllTenants() {
		return listTenantId;
	}

	/**
	 * Sets the default tenant identifier
	 * 
	 * @param tenantId the tenant identifier
	 */
	public void setCurrentTenantId(String tenantId) {
		if (!listTenantId.contains(tenantId)) {
			listTenantId.add(tenantId);
		}
		currentTenantId = tenantId;
	}

	/**
	 * Gets the default tenant identifier
	 * 
	 * @return the default tenant identifier
	 */
	public String getCurrentTenantId() {
		return currentTenantId;
	}

	/**
	 * Sets the default as null
	 */
	public void clearCurrentTenantId() {
		currentTenantId = null;
	}

	/**
	 * Adds a new tenant identifier, the system guarantees each tenant is unique
	 * 
	 * @param tenantId the new tenant identifier
	 */
	public void addTenant(String tenantId) {
		listTenantId.add(tenantId);
	}

	/**
	 * Deletes an specified tenant identifier, the system guarantees each tenant is
	 * unique
	 * If the current tenant is the one to remove, the current is set to null
	 * 
	 * @param tenantId the new tenant identifier
	 */
	public void removeTenant(String tenantId) {
		listTenantId.remove(tenantId);
		if (getCurrentTenantId().equals(tenantId)) {
			clearCurrentTenantId();
		}
	}
}
