package org.thalion.activiti.prototype.multischema;

import java.text.ParseException;

import org.activiti.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thalion.activiti.prototype.common.OnboardingRequestProcessorWithTenant;

/**
 * 
 * @author Angeles Broullon
 * 
 * Sets up an Activiti Multischema process engine
 *
 */
@Component
public class MultischemaPrototype {
	
	/**
	 * The log
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MultischemaPrototype.class);
	
	/**
	 * A single engine connected to several datasources
	 */	
	@Autowired
	private ProcessEngineMultiSchema pem;
	
	@Autowired
	private OnboardingRequestProcessorWithTenant processor;

	/**
	 * Constructor
	 *
	 */
	public MultischemaPrototype() {
	}

	/**
	 * @param engitenantIdneName
	 * @param workflowDefinition
	 * @return
	 */
	public ProcessDefinition deployWorkflow(final String tenantId, final String workflowDefinition) {
		return pem.deployWorkflowForTenant(workflowDefinition, tenantId);
	}

	/**
	 * Executs workflow deployed on given workflow engine with defined data set
	 * 
	 * @param engineName  workflow engine name in engine store
	 * @param processKey  workflow definition key
	 * @param dataSetName data set name / resource
	 * @return workflow processor
	 * @throws ParseException
	 */
	public OnboardingRequestProcessorWithTenant runLatestWorkflowWithKey(final String tenantId, final String processKey,
			final String dataSetName) throws ParseException {

		ProcessDefinition processDefinition = pem.findLatestProcessDefinition(tenantId, processKey);

		if (processDefinition == null) {
			LOGGER.error("Cannot find process definition with key: {} deployed for tenant: {}", processKey, tenantId);
			return null;
		}

		processor.runProcess(pem.getProcessEngine(), processDefinition, dataSetName);

		return processor;
	}

	/**
	 * Clears all the engine configuration
	 */
	public void clear() {
		pem.clearProcessEngine();		
	}

}
