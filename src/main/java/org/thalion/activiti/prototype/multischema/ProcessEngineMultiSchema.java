package org.thalion.activiti.prototype.multischema;

import javax.sql.DataSource;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.asyncexecutor.multitenant.ExecutorPerTenantAsyncExecutor;
import org.activiti.engine.impl.cfg.multitenant.MultiSchemaMultiTenantProcessEngineConfiguration;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.h2.jdbcx.JdbcDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Angeles Broullon
 * 
 *         A singleton process engine instance, to make the system lighter
 *
 */
@Component
public class ProcessEngineMultiSchema {

	/**
	 * An unique instance for the process engine
	 */
	private static ProcessEngine processEngine;

	/**
	 * The configuration
	 */
	private static MultiSchemaMultiTenantProcessEngineConfiguration configuration;
	
	/**
	 * InfoHolder, to be able to switch tenant when necessary
	 */
	private static CustomTenantInfoHolder infoHolder;

	/**
	 * Log
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessEngineMultiSchema.class);

	// Default H2 database configuration

	/**
	 * The database user
	 */
	private static final String USER = "sa";

	/**
	 * The database password
	 */
	private static final String PASS = "";

	/**
	 * Constructor
	 */
	public ProcessEngineMultiSchema() {
		setupProcessEngine(setupCustomTenantInfoHolder());
	}

	/**
	 * Generates the infoHolder
	 * 
	 * @return generates an infoHolder with default values
	 */
	private CustomTenantInfoHolder setupCustomTenantInfoHolder() {
		if (infoHolder == null) {
			infoHolder = new CustomTenantInfoHolder();
			infoHolder.loadDefaultCustomTenantInfoHolder();
		}		
		return infoHolder;
	}

	/**
	 * Gets the process engine
	 * 
	 * @return the process engine
	 */
	public ProcessEngine getProcessEngine() {
		if (processEngine == null) {
			setupProcessEngine(setupCustomTenantInfoHolder());
		}
		return processEngine;
	}

	/**
	 * Encapsulates the constructor, to ease the testing process by isolating it
	 * 
	 * @param tenantInfoHolder the info holder to set it up to
	 * @return  a newly createdpreocess engine  configuration
	 */
	protected MultiSchemaMultiTenantProcessEngineConfiguration setTenantInfoOnConfig(CustomTenantInfoHolder tenantInfoHolder) {
		return new MultiSchemaMultiTenantProcessEngineConfiguration(tenantInfoHolder);
	}

	/**
	 * Default configuration for the process engine
	 * 
	 * @param tenantInfoHolder the info holder to set it up to
	 */
	private void setupProcessEngine(CustomTenantInfoHolder tenantInfoHolder) {
		configuration = setTenantInfoOnConfig(tenantInfoHolder);

		configuration.setDatabaseType(MultiSchemaMultiTenantProcessEngineConfiguration.DATABASE_TYPE_H2);
		configuration.setDatabaseSchemaUpdate(MultiSchemaMultiTenantProcessEngineConfiguration.DB_SCHEMA_UPDATE_DROP_CREATE);

		configuration.setAsyncExecutorEnabled(true);
		configuration.setAsyncExecutorActivate(true);

		configuration.setAsyncExecutor(new ExecutorPerTenantAsyncExecutor(tenantInfoHolder));

		// register databases for tenants
		for (String tenant : tenantInfoHolder.getAllTenants()) {
			configuration.registerTenant(tenant, createDataSource(buildEngineDBUrl(tenant), USER, PASS));
		}

		processEngine = configuration.buildProcessEngine();
	}

	/**
	 * Deploys workflow process definition on to workflow engine with given name
	 * 
	 * @param processDefinitionResource workflow process definition resource name
	 * @param tenantId                  target tenant identifier
	 * @return workflow process definition entity reference
	 */
	public synchronized ProcessDefinition deployWorkflowForTenant(final String workflow, final String tenantId) {
		infoHolder.setCurrentTenantId(tenantId);
		RepositoryService repositoryService = processEngine.getRepositoryService();
		Deployment deployment = repositoryService.createDeployment().addClasspathResource(workflow).tenantId(tenantId)
				.deploy();
		ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
				.processDefinitionTenantId(tenantId).deploymentId(deployment.getId()).singleResult();

		LOGGER.info("Deployed process definition [{}] with id:[{}], key:[{}] for tenant: [{}]",
				processDefinition.getName(), processDefinition.getId(), processDefinition.getKey(), tenantId);

		return processDefinition;
	}

	/**
	 * Retrieves workflow process definition from engine with given name and process
	 * key.
	 * 
	 * @param engineName workflow engine name
	 * @param processKey process key
	 * @return process definition, otherwise null if not found
	 */
	public synchronized ProcessDefinition findLatestProcessDefinition(final String tenantId, final String processKey) {
		infoHolder.setCurrentTenantId(tenantId);
		RepositoryService repositoryService = processEngine.getRepositoryService();
		return repositoryService.createProcessDefinitionQuery().processDefinitionKey(processKey)
				.processDefinitionTenantId(tenantId).singleResult();
	}

	/**
	 * Clears all the engine configuration
	 */
	public synchronized void clearProcessEngine() {
		processEngine = null;
	}

	/**
	 * Helper method to construct in-memory engine store JDBC URL
	 * 
	 * @param userName database user name
	 * @return JDBC string
	 */
	private String buildEngineDBUrl(String userName) {
		StringBuilder builder = new StringBuilder();

		builder.append("jdbc:h2:mem:");
		builder.append(userName.toLowerCase());
		builder.append(";DB_CLOSE_DELAY=1000");

		return builder.toString();
	}

	private DataSource createDataSource(String jdbcUrl, String jdbcUsername, String jdbcPassword) {
		JdbcDataSource ds = new JdbcDataSource();
		ds.setURL(jdbcUrl);
		ds.setUser(jdbcUsername);
		ds.setPassword(jdbcPassword);
		return ds;
	}

}
