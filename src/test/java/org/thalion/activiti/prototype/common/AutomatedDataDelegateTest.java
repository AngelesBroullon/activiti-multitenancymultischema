package org.thalion.activiti.prototype.common;

import org.activiti.engine.delegate.DelegateExecution;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.activiti.prototype.common.AutomatedDataDelegate;

@RunWith(MockitoJUnitRunner.class)
public class AutomatedDataDelegateTest {
	
	private static final Object EXECUTION_NAME = "EXECUTION TESTING";

	@Mock
	private DelegateExecution execution;
	
	private AutomatedDataDelegate instance;
	
	@Before
	public void setUp() {
		instance = new AutomatedDataDelegate();
		Mockito.when(execution.getVariable(Matchers.anyString())).thenReturn(EXECUTION_NAME);		
	}
	
	@Test
	public void execute() throws Exception {
		instance.execute(execution);
		Mockito.verify(execution, Mockito.times(1)).getVariable(Mockito.anyString());
	}

}
