package org.thalion.activiti.prototype.common;

import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.FormType;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.impl.form.DateFormType;
import org.activiti.engine.impl.form.LongFormType;
import org.activiti.engine.impl.form.StringFormType;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.activiti.prototype.configuration.PrototypeConstants;

@RunWith(MockitoJUnitRunner.class)
public class OnboardingRequestProcessorTest {

	private static final String PROCESS_INSTANCE_ID = "PI_ID";
	private static final String PROCESS_INSTANCE_KEY = "PI_KEY";
	private static final String PROCESS_DEFINITION_ID = "PD_ID";
	private static final String START_EVENT = "Start event";

	private OnboardingRequestProcessor instance;

	@Mock
	private ProcessDefinition processDefinition;
	@Mock
	private ProcessEngine processEngine;
	@Mock
	private ProcessInstance processInstance;
	@Mock
	private RuntimeService runtimeService;
	@Mock
	private HistoryService historyService;
	@Mock
	private TaskService taskService;
	@Mock
	private FormService formService;
	@Mock
	private TaskQuery query;
	@Mock
	private Task task;
	@Mock
	private TaskFormData formData;
	@Mock
	private FormProperty formProperty;
	@Mock
	private HistoricActivityInstanceQuery historicActivityInstanceQuery;
	@Mock
	private HistoricActivityInstance historicActivityInstance;
	@Mock
	private ProcessInstanceQuery processInstanceQuery;
	@Mock
	private ProcessInstance processInstanceEnded;

	@Before
	public void setup() {
		instance = new OnboardingRequestProcessor();
		initMocks();
	}

	private void initMocks() {
		initMockProcessEngine();
		initMockRuntimeService();
		initMockProcessInstance();
		initMockProcessDefinition();
		initMockTaskService();
		initMockQuery();
		initMockFormService();
		initMockFormData();
		initMockFormType();
		initHistoryService();
		initMockHistoricActivityInstanceQuery();
		initMockHistoricActivityInstance();
		initMockProcessInstanceQuery();
		initMockProcessIntanceEnded();
	}

	private void  initMockProcessEngine() {
		Mockito.when(processEngine.getRuntimeService()).thenReturn(runtimeService);
		Mockito.when(processEngine.getTaskService()).thenReturn(taskService);
		Mockito.when(processEngine.getFormService()).thenReturn(formService);
		Mockito.when(processEngine.getHistoryService()).thenReturn(historyService);
	}
	
	private void initMockRuntimeService() {
		Mockito.when(runtimeService.startProcessInstanceByKey(Matchers.anyString())).thenReturn(processInstance);
		Mockito.when(runtimeService.createProcessInstanceQuery()).thenReturn(processInstanceQuery);
	}

	private void initMockProcessInstance() {
		Mockito.when(processInstance.getProcessInstanceId()).thenReturn(PROCESS_INSTANCE_ID);
		Mockito.when(processInstance.getProcessDefinitionKey()).thenReturn(PROCESS_INSTANCE_KEY);
	}

	private void initMockProcessDefinition() {
		Mockito.when(processDefinition.getId()).thenReturn(PROCESS_DEFINITION_ID);
	}

	private void initMockTaskService() {
		Mockito.when(taskService.createTaskQuery()).thenReturn(query);
	}

	private void initMockFormService() {
		Mockito.when(formService.getTaskFormData(Matchers.anyString())).thenReturn(formData);
	}

	private void initMockQuery() {
		Mockito.when(query.taskCandidateGroup(Matchers.anyString())).thenReturn(query);
		Mockito.when(query.list()).thenReturn(createDummyTaskList());
	}

	private void initMockFormData() {
		Mockito.when(formData.getFormProperties()).thenReturn(createDummyListFormProperties());
	}

	private void initMockFormType() {
		Mockito.when(formProperty.getType()).thenReturn(Mockito.mock(FormType.class));
	}

	private void initHistoryService() {
		Mockito.when(historyService.createHistoricActivityInstanceQuery()).thenReturn(historicActivityInstanceQuery);
	}
	
	private void initMockHistoricActivityInstanceQuery() {
		Mockito.when(historicActivityInstanceQuery.processInstanceId(Matchers.anyString())).thenReturn(historicActivityInstanceQuery);
		Mockito.when(historicActivityInstanceQuery.finished()).thenReturn(historicActivityInstanceQuery);
		Mockito.when(historicActivityInstanceQuery.orderByHistoricActivityInstanceEndTime()).thenReturn(historicActivityInstanceQuery);
		Mockito.when(historicActivityInstanceQuery.asc()).thenReturn(historicActivityInstanceQuery);
		Mockito.when(historicActivityInstanceQuery.list()).thenReturn(createDummyHistoricActivityInstance());
	}
	
	private void initMockHistoricActivityInstance() {
		Mockito.when(historicActivityInstance.getActivityType()).thenReturn(START_EVENT);	
	}
	
	private void initMockProcessInstanceQuery() {
		Mockito.when(processInstanceQuery.processInstanceId(Matchers.anyString())).thenReturn(processInstanceQuery);
		Mockito.when(processInstanceQuery.singleResult()).thenReturn(processInstanceEnded);		
	}
	
	private void initMockProcessIntanceEnded() {
		Mockito.when(processInstanceEnded.isEnded()).thenReturn(true);		
	}

	private List<Task> createDummyTaskList() {
		List<Task> list = new LinkedList<>();
		list.add(task);
		return list;
	}

	private List<FormProperty> createDummyListFormProperties() {
		List<FormProperty> list = new LinkedList<>();
		list.add(formProperty);
		return list;
	}
	
	private List<HistoricActivityInstance> createDummyHistoricActivityInstance() {
		List<HistoricActivityInstance> list = new LinkedList<>();
		list.add(historicActivityInstance);
		return list;
	}

	@Test
	public void getProcessInstance() {
		Assert.assertEquals(processInstance, instance.getProcessInstance(processDefinition, runtimeService));
	}
	
	@Test
	public void runProcess_nullProcessInstance() throws ParseException {
		Mockito.when(runtimeService.startProcessInstanceByKey(Matchers.anyString())).thenReturn(null);
		instance.runProcess(processEngine, processDefinition, PrototypeConstants.FIRST_DATA_SET);
		Mockito.verify(runtimeService, Mockito.times(0)).createProcessInstanceQuery();
	}
	
	@Test
	public void runProcess_typeNotSupported() throws ParseException {
		instance.runProcess(processEngine, processDefinition, PrototypeConstants.FIRST_DATA_SET);
		Mockito.verify(runtimeService, Mockito.times(1)).createProcessInstanceQuery();
	}
	
	@Test
	public void runProcess_typeStringFormType() throws ParseException {
		Mockito.when(formProperty.getType()).thenReturn(Mockito.mock(StringFormType.class));
		instance.runProcess(processEngine, processDefinition, PrototypeConstants.FIRST_DATA_SET);
		Mockito.verify(runtimeService, Mockito.times(1)).createProcessInstanceQuery();
	}
	
	@Test
	public void runProcess_typeLongFormType() throws ParseException {
		Mockito.when(formProperty.getType()).thenReturn(Mockito.mock(LongFormType.class));
		instance.runProcess(processEngine, processDefinition, PrototypeConstants.LONG_DATA_SET);
		Mockito.verify(runtimeService, Mockito.times(1)).createProcessInstanceQuery();
	}
	
	@Test
	public void runProcess_typeDateFormType() throws ParseException {
		Mockito.when(formProperty.getType()).thenReturn(Mockito.mock(DateFormType.class));
		instance.runProcess(processEngine, processDefinition, PrototypeConstants.DATE_DATA_SET);
		Mockito.verify(runtimeService, Mockito.times(1)).createProcessInstanceQuery();
	}
	

}
