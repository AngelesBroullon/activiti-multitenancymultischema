package org.thalion.activiti.prototype.common;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OnboardingRequestProcessorWithTenantTest {

	@Mock
	private ProcessDefinition processDefinition;
	@Mock
	private RuntimeService runtimeService;
	@Mock
	private ProcessInstance processInstance;

	private OnboardingRequestProcessorWithTenant instance;

	@Before
	public void setUp() {
		instance = new OnboardingRequestProcessorWithTenant();
		Mockito.when(runtimeService.startProcessInstanceByKeyAndTenantId(Matchers.anyString(), Matchers.anyString()))
				.thenReturn(processInstance);
	}

	@Test
	public void getProcessInstance() {
		Assert.assertEquals(processInstance, instance.getProcessInstance(processDefinition, runtimeService));
	}

}
