package org.thalion.activiti.prototype.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "org.thalion.activiti.prototype")
public class PrototypeConfiguration {

}
