package org.thalion.activiti.prototype.configuration;

public class PrototypeConstants {
	
	/**
	 * First Activiti engine user name
	 */
	public static final String FIRST_ENGINE_NAME = "ACTIVITI_1";

	/**
	 * Second Activiti engine user name
	 */
	public static final String SECOND_ENGINE_NAME = "ACTIVITI_2";

	/**
	 * Onboarding process definition (v1)
	 */
	public static final String ONBOARDING_WF_V1_DEF = "onboarding_v1.bpmn20.xml";

	/**
	 * Onborading wokflow id (v1)
	 */
	public static final String ONBOARDING_WF_V1_KEY = "onboarding_v1";

	/**
	 * On boarding process definition v1
	 */
	public static final String ONBOARDING_WF_V2_DEF = "onboarding_v2.bpmn20.xml";

	/**
	 * Onborading wokflow id (v1)
	 */
	public static final String ONBOARDING_WF_V2_KEY = "onboarding_v2";

	/**
	 * Test data set #1
	 */
	public static final String FIRST_DATA_SET = "onboarding_data_1.txt";

	/**
	 * Test data set #2
	 */
	public static final String SECOND_DATA_SET = "onboarding_data_2.txt";
	
	/**
	 * Test data set #2
	 */
	public static final String DATE_DATA_SET = "onboarding_data_date.txt";
	
	/**
	 * Test data set #2
	 */
	public static final String LONG_DATA_SET = "onboarding_data_long.txt";

}
