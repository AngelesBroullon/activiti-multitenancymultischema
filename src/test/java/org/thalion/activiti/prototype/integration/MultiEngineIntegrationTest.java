package org.thalion.activiti.prototype.integration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.thalion.activiti.prototype.configuration.PrototypeConstants;
import org.thalion.activiti.prototype.multiengine.MultienginePrototype;

/**
 * Inherited class
 * 
 * Poc test class. Testing activiti engine cotenancy.
 * 
 */
@Configuration
@ComponentScan(basePackages = { "org.thalion.activiti.prototype.common", "org.thalion.activiti.prototype.multiengine" })
public class MultiEngineIntegrationTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(MultiEngineIntegrationTest.class);

	private AnnotationConfigApplicationContext ctx;;
	
	private MultienginePrototype testInstance;

	@Before
	public void setUp() {
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(MultiEngineIntegrationTest.class);
		ctx.refresh();
		testInstance = (MultienginePrototype) ctx.getBean("multienginePrototype");
		
		testInstance.addEngine(PrototypeConstants.FIRST_ENGINE_NAME);
		testInstance.addEngine(PrototypeConstants.SECOND_ENGINE_NAME);
	}
	
	@After
	public void tearDown() {
		testInstance.clearEngines();
	}

	/**
	 * Test method for
	 * {@link eu.europa.ema.poc.activiti.multitenancy.Poc#runLatestWorkflowWithKey(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testRunWorkflow_OneEngine_FirstWorkflow_2DataSets() throws Exception {
		LOGGER.info("testRunWorkflow: First engine, First workflow, 2 dataSets");
		String engineName = PrototypeConstants.FIRST_ENGINE_NAME;

		// deploy workflow definitions
		assertNotNull(this.testInstance.deployWorkflow(engineName, PrototypeConstants.ONBOARDING_WF_V1_DEF));

		// run non existent workflow
		assertNull(this.testInstance.runLatestWorkflowWithKey(engineName, PrototypeConstants.ONBOARDING_WF_V2_KEY,
				PrototypeConstants.FIRST_DATA_SET));

		// run existent workflow with both data sets
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(engineName, PrototypeConstants.ONBOARDING_WF_V1_KEY,
				PrototypeConstants.FIRST_DATA_SET));
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(engineName, PrototypeConstants.ONBOARDING_WF_V1_KEY,
				PrototypeConstants.SECOND_DATA_SET));
	}

	/**
	 * Test method for
	 * {@link eu.europa.ema.poc.activiti.multitenancy.Poc#runLatestWorkflowWithKey(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testRunWorkflow_OneEngine_SecondWorkflow_2DataSets() throws Exception {
		LOGGER.info("test Run Workflow, FirstEngine Second Workflow, 2 dataSets");
		String engineName = PrototypeConstants.SECOND_ENGINE_NAME;
		;
		// deploy workflow definitions
		assertNotNull(this.testInstance.deployWorkflow(engineName, PrototypeConstants.ONBOARDING_WF_V2_DEF));

		// run non existent workflow
		assertNull(this.testInstance.runLatestWorkflowWithKey(engineName, PrototypeConstants.ONBOARDING_WF_V1_KEY,
				PrototypeConstants.FIRST_DATA_SET));

		// run existent workflow with both data sets
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(engineName, PrototypeConstants.ONBOARDING_WF_V2_KEY,
				PrototypeConstants.FIRST_DATA_SET));
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(engineName, PrototypeConstants.ONBOARDING_WF_V2_KEY,
				PrototypeConstants.SECOND_DATA_SET));
	}

	/**
	 * Test method for
	 * {@link eu.europa.ema.poc.activiti.multitenancy.Poc#runLatestWorkflowWithKey(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testRunWorkflow_2Engines_different_workflows_allDataSets() throws Exception {

		// deploy workflow to each engine
		assertNotNull(this.testInstance.deployWorkflow(PrototypeConstants.FIRST_ENGINE_NAME,
				PrototypeConstants.ONBOARDING_WF_V1_DEF));
		assertNotNull(this.testInstance.deployWorkflow(PrototypeConstants.SECOND_ENGINE_NAME,
				PrototypeConstants.ONBOARDING_WF_V2_DEF));

		// run all data sets on first engine
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(PrototypeConstants.FIRST_ENGINE_NAME,
				PrototypeConstants.ONBOARDING_WF_V1_KEY, PrototypeConstants.FIRST_DATA_SET));
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(PrototypeConstants.FIRST_ENGINE_NAME,
				PrototypeConstants.ONBOARDING_WF_V1_KEY, PrototypeConstants.SECOND_DATA_SET));
		assertNull(this.testInstance.runLatestWorkflowWithKey(PrototypeConstants.FIRST_ENGINE_NAME,
				PrototypeConstants.ONBOARDING_WF_V2_KEY, PrototypeConstants.FIRST_DATA_SET));

		// run all data sets on second engine
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(PrototypeConstants.SECOND_ENGINE_NAME,
				PrototypeConstants.ONBOARDING_WF_V2_KEY, PrototypeConstants.FIRST_DATA_SET));
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(PrototypeConstants.SECOND_ENGINE_NAME,
				PrototypeConstants.ONBOARDING_WF_V2_KEY, PrototypeConstants.SECOND_DATA_SET));
		assertNull(this.testInstance.runLatestWorkflowWithKey(PrototypeConstants.SECOND_ENGINE_NAME,
				PrototypeConstants.ONBOARDING_WF_V1_KEY, PrototypeConstants.FIRST_DATA_SET));
	}

}
