package org.thalion.activiti.prototype.integration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.thalion.activiti.prototype.configuration.PrototypeConstants;
import org.thalion.activiti.prototype.multischema.CustomTenantInfoHolder;
import org.thalion.activiti.prototype.multischema.MultischemaPrototype;

@Configuration
@ComponentScan(basePackages = { "org.thalion.activiti.prototype.common", "org.thalion.activiti.prototype.multischema" })
public class MultischemaIntegrationTest {

	private MultischemaPrototype testInstance;

	private AnnotationConfigApplicationContext ctx;

	@Before
	public void setUp() {
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(MultischemaIntegrationTest.class);
		ctx.refresh();
		testInstance = (MultischemaPrototype) ctx.getBean("multischemaPrototype");
	}

	@After
	public void tearDown() {
		testInstance.clear();
	}

	/**
	 * Test method for
	 * {@link eu.europa.ema.poc.activiti.multitenancy.Poc#runLatestWorkflowWithKey(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testRunWorkflow_OneTenant_FirstWorkflow_2DataSets() throws Exception {
		// deploy workflow definitions
		assertNotNull(this.testInstance.deployWorkflow(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V1_DEF));

		// run non existent workflow
		assertNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V2_KEY, PrototypeConstants.FIRST_DATA_SET));

		// run existent workflow with both data sets
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V1_KEY, PrototypeConstants.FIRST_DATA_SET));
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V1_KEY, PrototypeConstants.SECOND_DATA_SET));

	}

	/**
	 * Test method for
	 * {@link eu.europa.ema.poc.activiti.multitenancy.Poc#runLatestWorkflowWithKey(java.lang.String, java.lang.String, java.lang.String)}.
	 */

	@Test
	public void testRunWorkflow_OneTenant_SecondWorkflow_2DataSets() throws Exception {
		// deploy workflow definitions
		assertNotNull(this.testInstance.deployWorkflow(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V2_DEF));

		// run non existent workflow
		assertNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V1_KEY, PrototypeConstants.FIRST_DATA_SET));

		// run existent workflow with both data sets
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V2_KEY, PrototypeConstants.FIRST_DATA_SET));
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V2_KEY, PrototypeConstants.SECOND_DATA_SET));
	}

	/**
	 * Test method for
	 * {@link eu.europa.ema.poc.activiti.multitenancy.Poc#runLatestWorkflowWithKey(java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testRunWorkflow_2ETenants_different_workflows_allDataSets() throws Exception {

		// deploy workflow to each engine
		assertNotNull(this.testInstance.deployWorkflow(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V1_DEF));
		assertNotNull(this.testInstance.deployWorkflow(CustomTenantInfoHolder.TENANT_2,
				PrototypeConstants.ONBOARDING_WF_V2_DEF));

		// run all data sets on first engine
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V1_KEY, PrototypeConstants.FIRST_DATA_SET));
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V1_KEY, PrototypeConstants.SECOND_DATA_SET));
		assertNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V2_KEY, PrototypeConstants.FIRST_DATA_SET));

		// run all data sets on second engine
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_2,
				PrototypeConstants.ONBOARDING_WF_V2_KEY, PrototypeConstants.FIRST_DATA_SET));
		assertNotNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_2,
				PrototypeConstants.ONBOARDING_WF_V2_KEY, PrototypeConstants.SECOND_DATA_SET));
		assertNull(this.testInstance.runLatestWorkflowWithKey(CustomTenantInfoHolder.TENANT_2,
				PrototypeConstants.ONBOARDING_WF_V1_KEY, PrototypeConstants.FIRST_DATA_SET));
	}

}
