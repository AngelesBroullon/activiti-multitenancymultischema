package org.thalion.activiti.prototype.multiengine;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.activiti.prototype.common.OnboardingRequestProcessor;
import org.thalion.activiti.prototype.configuration.PrototypeConstants;

@RunWith(MockitoJUnitRunner.class)
public class MultienginePrototypeTest {

	private static final String TENANT = "ACTIVITI-TENANT";
	private static final String WORKFLOW = "TEST_WORKFLOW";
	private static final String PROCESS_KEY = "PROCESS_KEY";
	private static final String DATASET_NAME = "TEST_DATASET";

	@InjectMocks
	private MultienginePrototype instance;
	@Mock
	private ProcessEngineStorage engineStorage;
	@Mock
	private OnboardingRequestProcessor processor;

	@Mock
	private ProcessDefinition processDefinition;
	@Mock
	private ProcessEngine processEngine;
	@Mock
	private ProcessInstance processInstance;

	@Before
	public void setUp() {
		initMockEngineStorage();
		initMockProcessor();
	}

	private void initMockEngineStorage() {
		Mockito.when(engineStorage.getEngineNames()).thenReturn(createDummySetString());
		Mockito.when(engineStorage.getProcessEngine(Matchers.anyString())).thenReturn(processEngine);
		Mockito.when(engineStorage.deployEngineProcess(Matchers.anyString(), Matchers.anyString()))
				.thenReturn(processDefinition);
		Mockito.when(engineStorage.findLatestProcessDefinition(Matchers.anyString(), Matchers.anyString()))
				.thenReturn(processDefinition);		
	}
	
	private void initMockProcessor() {
		Mockito.when(
				processor.getProcessInstance(Matchers.any(ProcessDefinition.class), Matchers.any(RuntimeService.class)))
				.thenReturn(processInstance);
	}

	private Set<String> createDummySetString() {
		Set<String> set = new HashSet<>();
		set.add(PrototypeConstants.FIRST_ENGINE_NAME);
		set.add(PrototypeConstants.SECOND_ENGINE_NAME);
		return set;
	}
	
	@Test
	public void addEngine() {
		instance.addEngine(PrototypeConstants.FIRST_ENGINE_NAME);
		Mockito.verify(engineStorage, Mockito.times(1)).addEngine(Matchers.eq(PrototypeConstants.FIRST_ENGINE_NAME));;	
	}
	
	@Test
	public void clearEngines() {
		instance.clearEngines();
		Mockito.verify(engineStorage, Mockito.times(1)).clear();	
	}

	@Test
	public void getEngineNames() {
		Assert.assertEquals(createDummySetString(), instance.getEngineNames());
	}

	@Test
	public void deployWorkflow() {
		Assert.assertEquals(processDefinition, instance.deployWorkflow(WORKFLOW, TENANT));
	}

	@Test
	public void runLatestWorkflowWithKey_notNullProcessDefinition() throws ParseException {
		Assert.assertEquals(processor, instance.runLatestWorkflowWithKey(TENANT, PROCESS_KEY, DATASET_NAME));
	}

	@Test
	public void runLatestWorkflowWithKey_nullProcessDefinition() throws ParseException {
		Mockito.when(engineStorage.findLatestProcessDefinition(Matchers.anyString(), Matchers.anyString()))
				.thenReturn(null);
		Assert.assertNull(instance.runLatestWorkflowWithKey(TENANT, PROCESS_KEY, DATASET_NAME));
	}

}
