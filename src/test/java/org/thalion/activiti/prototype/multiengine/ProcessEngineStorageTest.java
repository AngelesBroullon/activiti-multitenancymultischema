package org.thalion.activiti.prototype.multiengine;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.activiti.prototype.configuration.PrototypeConstants;

@RunWith(MockitoJUnitRunner.class)
public class ProcessEngineStorageTest {

	private static final String MOCKED_NAME_1 = "ENGINE 1";
	private static final String MOCKED_NAME_2 = "ENGINE 2";
	private static final String DEPLOYMENT_ID = "DEPLOYMENT_1";
	private static final String PROCESS_DEFINITION_NAME = "PROCESS TEST FOR ACTIVITI";
	private static final String PROCESS_DEFINITION_ID = "PROCESS TEST";
	private static final String PROCESS_DEFINITION_KEY = "P_TEST";

	@InjectMocks
	private ProcessEngineStorage instance;
	@Mock
	private StandaloneProcessEngineConfiguration processEngineConfiguration;

	@Mock
	private ProcessEngine processEngine;
	@Mock
	private RepositoryService repositoryService;
	@Mock
	private DeploymentBuilder deploymentBuilder;
	@Mock
	private Deployment deployment;
	@Mock
	private ProcessDefinitionQuery processDefinitionQuery;
	@Mock
	private ProcessDefinition processDefinition;

	@Before
	public void setup() {
		initMocks();
		instance.addEngine(MOCKED_NAME_1);
	}

	private void initMocks() {
		initMockProcessEngineConfiguration();
		initMockProcessEngine();
		initMockRepositoryService();
		initMockDeploymentBuilder();
		initMockProcessDefinitionquery();
		initMockDeployment();
		initMockProcessDefinition();
	}

	private void initMockProcessEngineConfiguration() {
		Mockito.when(processEngineConfiguration.setJdbcUrl(Matchers.anyString()))
				.thenReturn(processEngineConfiguration);
		Mockito.when(processEngineConfiguration.setJdbcUsername(Matchers.anyString()))
				.thenReturn(processEngineConfiguration);
		Mockito.when(processEngineConfiguration.setJdbcPassword(Matchers.anyString()))
				.thenReturn(processEngineConfiguration);
		Mockito.when(processEngineConfiguration.setJdbcDriver(Matchers.anyString()))
				.thenReturn(processEngineConfiguration);
		Mockito.when(processEngineConfiguration.setDatabaseSchemaUpdate(Matchers.anyString()))
				.thenReturn(processEngineConfiguration);
		Mockito.when(processEngineConfiguration.buildProcessEngine()).thenReturn(processEngine);
	}

	private void initMockProcessEngine() {
		Mockito.when(processEngine.getRepositoryService()).thenReturn(repositoryService);
	}

	private void initMockRepositoryService() {
		Mockito.when(repositoryService.createDeployment()).thenReturn(deploymentBuilder);
		Mockito.when(repositoryService.createProcessDefinitionQuery()).thenReturn(processDefinitionQuery);
	}

	private void initMockDeploymentBuilder() {
		Mockito.when(deploymentBuilder.addClasspathResource(Matchers.anyString())).thenReturn(deploymentBuilder);
		Mockito.when(deploymentBuilder.deploy()).thenReturn(deployment);
	}

	private void initMockProcessDefinitionquery() {
		Mockito.when(processDefinitionQuery.deploymentId(Matchers.anyString())).thenReturn(processDefinitionQuery);
		Mockito.when(processDefinitionQuery.processDefinitionKey(Matchers.anyString())).thenReturn(processDefinitionQuery);
		Mockito.when(processDefinitionQuery.singleResult()).thenReturn(processDefinition);
		Mockito.when(processDefinitionQuery.list()).thenReturn(createListProcessDefinition());
	}

	private void initMockDeployment() {
		Mockito.when(deployment.getId()).thenReturn(DEPLOYMENT_ID);		
	}
	
	private void initMockProcessDefinition() {
		Mockito.when(processDefinition.getName()).thenReturn(PROCESS_DEFINITION_NAME);	
		Mockito.when(processDefinition.getId()).thenReturn(PROCESS_DEFINITION_ID);		
		Mockito.when(processDefinition.getKey()).thenReturn(PROCESS_DEFINITION_KEY);	
	}
	
	private List<ProcessDefinition> createListProcessDefinition() {
		List<ProcessDefinition> list = new LinkedList<ProcessDefinition>();
		list.add(processDefinition);
		return list;
	}

	@Test
	public void addEngine_newEngine() {
		instance.addEngine(MOCKED_NAME_2);
		Set<String> names = instance.getEngineNames();
		Assert.assertTrue(names.contains(MOCKED_NAME_2));
		Assert.assertTrue(names.size() > 1);
		Assert.assertNotNull(instance.getProcessEngine(MOCKED_NAME_2));
	}

	@Test
	public void addEngine_existingengine() {
		instance.addEngine(MOCKED_NAME_1);
		Set<String> names = instance.getEngineNames();
		Assert.assertTrue(names.contains(MOCKED_NAME_1));
		Assert.assertTrue(names.size() == 1);
		Assert.assertNotNull(instance.getProcessEngine(MOCKED_NAME_1));
	}

	@Test
	public void deployEngineProcess_newProcessDefinition() {
		ProcessDefinition processDefintion = instance.deployEngineProcess(MOCKED_NAME_1,
				PrototypeConstants.ONBOARDING_WF_V1_DEF);
		Assert.assertNotNull(processDefintion);
	}

	@Test
	public void deployEngineProcess_nonExistingEngine() {
		ProcessDefinition processDefintion = instance.deployEngineProcess(MOCKED_NAME_2,
				PrototypeConstants.ONBOARDING_WF_V1_DEF);
		Assert.assertNull(processDefintion);
	}


	@Test
	public void findLatestProcessDefinition_existingEngine() {
		ProcessDefinition processDefintion = instance.deployEngineProcess(MOCKED_NAME_1,
				PrototypeConstants.ONBOARDING_WF_V2_DEF);
		ProcessDefinition processDefintion2 = instance.findLatestProcessDefinition(MOCKED_NAME_1,
				processDefintion.getKey());
		Assert.assertNotNull(processDefintion2);
	}

	@Test
	public void findLatestProcessDefinition_nonExistingEngine() {
		ProcessDefinition processDefintion2 = instance.findLatestProcessDefinition(MOCKED_NAME_2,
				PrototypeConstants.ONBOARDING_WF_V2_KEY);
		Assert.assertNull(processDefintion2);
	}

	@Test
	public void getEngineNames() {
		Assert.assertTrue(instance.getEngineNames().contains(MOCKED_NAME_1));
	}

	@Test
	public void getProcessEngine() {
		ProcessEngine engine = instance.getProcessEngine(MOCKED_NAME_1);
		Assert.assertNotNull(engine);
	}

	@Test
	public void clear() {
		instance.clear();
		Assert.assertTrue(instance.getEngineNames().isEmpty());
	}

}
