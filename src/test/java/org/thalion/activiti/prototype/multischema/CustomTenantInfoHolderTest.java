package org.thalion.activiti.prototype.multischema;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.thalion.activiti.prototype.multischema.CustomTenantInfoHolder;

public class CustomTenantInfoHolderTest {

	public static final String TENANT_3 = "ACTIVITI-3";
	
	private CustomTenantInfoHolder instance;

	@Before
	public void setUp() {
		instance = new CustomTenantInfoHolder();
	}

	@Test
	public void loadDefaultCustomTenantInfoHolder() {
		instance.loadDefaultCustomTenantInfoHolder();
		assertTrue(instance.getAllTenants().contains(CustomTenantInfoHolder.TENANT_1));
		assertTrue(instance.getAllTenants().contains(CustomTenantInfoHolder.TENANT_2));
		assertEquals(CustomTenantInfoHolder.TENANT_1, instance.getCurrentTenantId());
	}
	
	@Test
	public void getAllTenants_emptyAfterCreationData() {
		Collection<String> list = instance.getAllTenants();
		assertTrue(list.isEmpty());
	}
	
	@Test
	public void getAllTenants_defaultData() {
		instance.loadDefaultCustomTenantInfoHolder();
		Collection<String> list = instance.getAllTenants();
		assertTrue(list.contains(CustomTenantInfoHolder.TENANT_1));
		assertTrue(list.contains(CustomTenantInfoHolder.TENANT_2));
	}
	
	@Test
	public void setCurrentTenantId_defaultLoadTenant() {
		instance.loadDefaultCustomTenantInfoHolder();
		instance.setCurrentTenantId(CustomTenantInfoHolder.TENANT_1);
		assertEquals(CustomTenantInfoHolder.TENANT_1, instance.getCurrentTenantId());
	}
	
	@Test
	public void setCurrentTenantId_addNewTenant() {
		instance.setCurrentTenantId(TENANT_3);
		assertEquals(TENANT_3, instance.getCurrentTenantId());
		assertTrue(instance.getAllTenants().contains(TENANT_3));
	}
	
	@Test
	public void getCurrentTenantId() {
		instance.loadDefaultCustomTenantInfoHolder();
		assertEquals(CustomTenantInfoHolder.TENANT_1, instance.getCurrentTenantId());
	}
	
	@Test
	public void clearTenantId() {
		instance.loadDefaultCustomTenantInfoHolder();
		instance.clearCurrentTenantId();
		assertNull(instance.getCurrentTenantId());
	}
	
	@Test
	public void addTenant() {
		instance.addTenant(TENANT_3);
		assertTrue(instance.getAllTenants().contains(TENANT_3));
		assertNotEquals(TENANT_3, instance.getCurrentTenantId());
	}
	
	@Test
	public void removeTenant_removeCurrentTenant() {
		instance.loadDefaultCustomTenantInfoHolder();
		instance.removeTenant(CustomTenantInfoHolder.TENANT_1);
		assertFalse(instance.getAllTenants().contains(CustomTenantInfoHolder.TENANT_1));
		assertNull(instance.getCurrentTenantId());
	}
	
	@Test
	public void removeTenant_removeNotCurrentTenant() {
		instance.loadDefaultCustomTenantInfoHolder();
		instance.removeTenant(CustomTenantInfoHolder.TENANT_2);
		assertFalse(instance.getAllTenants().contains(CustomTenantInfoHolder.TENANT_2));
		assertFalse(instance.getCurrentTenantId().equals(CustomTenantInfoHolder.TENANT_2));
	}

}
