package org.thalion.activiti.prototype.multischema;

import java.text.ParseException;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.activiti.prototype.common.OnboardingRequestProcessorWithTenant;
import org.thalion.activiti.prototype.multischema.MultischemaPrototype;
import org.thalion.activiti.prototype.multischema.ProcessEngineMultiSchema;

@RunWith(MockitoJUnitRunner.class)
public class MultischemaPrototypeTest {

	private static final String TENANT = "ACTIVITI-TENANT";
	private static final String WORKFLOW = "TEST_WORKFLOW";
	private static final String PROCESS_KEY = "PROCESS_KEY";
	private static final String DATASET_NAME = "TEST_DATASET";

	@Mock
	private ProcessEngineMultiSchema pem;
	@Mock
	private OnboardingRequestProcessorWithTenant processor;

	@InjectMocks
	private MultischemaPrototype instance;

	@Mock
	private ProcessDefinition processDefinition;
	@Mock
	private ProcessEngine processEngine;
	@Mock
	private ProcessInstance processInstance;

	@Before
	public void setUp() {
		Mockito.when(pem.deployWorkflowForTenant(Matchers.anyString(), Matchers.anyString()))
				.thenReturn(processDefinition);
		Mockito.when(pem.findLatestProcessDefinition(Matchers.anyString(), Matchers.anyString()))
				.thenReturn(processDefinition);
		Mockito.when(pem.getProcessEngine()).thenReturn(processEngine);

		Mockito.when(
				processor.getProcessInstance(Matchers.any(ProcessDefinition.class), Matchers.any(RuntimeService.class)))
				.thenReturn(processInstance);
	}

	@Test
	public void deployWorkflow() {
		Assert.assertEquals(processDefinition, instance.deployWorkflow(WORKFLOW, TENANT));
	}

	@Test
	public void runLatestWorkflowWithKey_notNullProcessDefinition() throws ParseException {
		Assert.assertEquals(processor, instance.runLatestWorkflowWithKey(TENANT, PROCESS_KEY, DATASET_NAME));
	}

	@Test
	public void runLatestWorkflowWithKey_nullProcessDefinition() throws ParseException {
		Mockito.when(pem.findLatestProcessDefinition(Matchers.anyString(), Matchers.anyString())).thenReturn(null);
		Assert.assertNull(instance.runLatestWorkflowWithKey(TENANT, PROCESS_KEY, DATASET_NAME));
	}

	@Test
	public void clear() {
		instance.clear();
		Mockito.verify(pem, Mockito.times(1)).clearProcessEngine();
	}

}
