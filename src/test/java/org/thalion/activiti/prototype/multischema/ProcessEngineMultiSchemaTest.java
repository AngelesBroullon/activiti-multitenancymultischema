package org.thalion.activiti.prototype.multischema;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.impl.cfg.multitenant.MultiSchemaMultiTenantProcessEngineConfiguration;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.thalion.activiti.prototype.configuration.PrototypeConstants;

public class ProcessEngineMultiSchemaTest {

	private static final String MOCKED_ID = "identity";
	private static final String MOCKED_KEY = "key value";
	private static final String MOCKED_NAME = "my name mock";

	private ProcessEngineMultiSchemaMockConfig instance;

	private ProcessEngine engineMock;
	private MultiSchemaMultiTenantProcessEngineConfiguration configMock;
	private RepositoryService repositoryServiceMock;
	private Deployment deploymentMock;
	private ProcessDefinition processDefinitionMock;

	/**
	 * Enables to handle the configuration, hence mocking it without using
	 * PowerMockito
	 *
	 */
	private class ProcessEngineMultiSchemaMockConfig extends ProcessEngineMultiSchema {

		@Override
		protected MultiSchemaMultiTenantProcessEngineConfiguration setTenantInfoOnConfig(
				CustomTenantInfoHolder tenantInfoHolder) {
			configMock = Mockito.mock(MultiSchemaMultiTenantProcessEngineConfiguration.class);
			engineMock = Mockito.mock(ProcessEngine.class);
			repositoryServiceMock = Mockito.mock(RepositoryService.class);
			deploymentMock = Mockito.mock(Deployment.class);
			processDefinitionMock = Mockito.mock(ProcessDefinition.class);
			setupCommonMock();
			return configMock;
		}
	}

	@Before
	public void setUp() throws Exception {
		instance = new ProcessEngineMultiSchemaMockConfig();

		DeploymentBuilder deploymentBuilderMock = Mockito.mock(DeploymentBuilder.class);
		ProcessDefinitionQuery processQueryMock = Mockito.mock(ProcessDefinitionQuery.class);

		Mockito.when(configMock.buildProcessEngine()).thenReturn(engineMock);
		Mockito.when(engineMock.getRepositoryService()).thenReturn(repositoryServiceMock);
		Mockito.when(repositoryServiceMock.createDeployment()).thenReturn(deploymentBuilderMock);
		Mockito.when(deploymentBuilderMock.addClasspathResource(Matchers.anyString()))
				.thenReturn(deploymentBuilderMock);
		Mockito.when(deploymentBuilderMock.tenantId(Matchers.anyString())).thenReturn(deploymentBuilderMock);
		Mockito.when(deploymentBuilderMock.deploy()).thenReturn(deploymentMock);
		Mockito.when(repositoryServiceMock.createProcessDefinitionQuery()).thenReturn(processQueryMock);
		Mockito.when(processQueryMock.processDefinitionTenantId(Matchers.anyString())).thenReturn(processQueryMock);
		Mockito.when(processQueryMock.deploymentId(Matchers.anyString())).thenReturn(processQueryMock);
		Mockito.when(processQueryMock.singleResult()).thenReturn(processDefinitionMock);
		Mockito.when(processQueryMock.processDefinitionKey(Matchers.anyString())).thenReturn(processQueryMock);

		setupCommonMock();
	}

	private void setupCommonMock() {
		Mockito.when(configMock.buildProcessEngine()).thenReturn(engineMock);
		Mockito.when(processDefinitionMock.getId()).thenReturn(MOCKED_ID);
		Mockito.when(processDefinitionMock.getKey()).thenReturn(MOCKED_KEY);
		Mockito.when(processDefinitionMock.getName()).thenReturn(MOCKED_NAME);
	}

	@Test
	public void getProcessEngine_fromScratch() {
		instance.clearProcessEngine();
		ProcessEngine engine = instance.getProcessEngine();
		assertNotNull(engine);
	}

	@Test
	public void getProcessEngine_sameInstanceForBoth() {
		ProcessEngine engine1 = instance.getProcessEngine();
		ProcessEngine engine2 = instance.getProcessEngine();
		assertEquals(engine1, engine2);
	}

	@Test
	public void clearProcessEngine() {
		instance.clearProcessEngine();
		ProcessEngine engine = (ProcessEngine) getField(instance, "processEngine");
		assertNull(engine);
	}

	private Object getField(Object objectInstance, String fieldName) {
		Field fields[] = objectInstance.getClass().getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			if (field.getName().equals(fieldName)) {
				try {
					return field.get(objectInstance);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	@Test
	public void deployWorkflowForTenant() {
		ProcessDefinition definition = instance.deployWorkflowForTenant(PrototypeConstants.ONBOARDING_WF_V1_DEF,
				CustomTenantInfoHolder.TENANT_1);
		assertEquals(MOCKED_NAME, definition.getName());
		assertEquals(MOCKED_ID, definition.getId());
		assertEquals(MOCKED_KEY, definition.getKey());

	}

	@Test
	public void findLatestProcessDefinition() {
		ProcessDefinition definition = instance.findLatestProcessDefinition(CustomTenantInfoHolder.TENANT_1,
				PrototypeConstants.ONBOARDING_WF_V1_DEF);
		assertEquals(MOCKED_NAME, definition.getName());
		assertEquals(MOCKED_ID, definition.getId());
		assertEquals(MOCKED_KEY, definition.getKey());
	}

}
